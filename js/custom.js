
var $shapeIndex = 0;
var $shapesLibrary = [];


function updateEditForm(selectedValue) {

    if($('#shapeProperties').length != 0) {
        $('#shapeProperties').remove();
    }
    var $newDiv;
    if(selectedValue == 'Quadrat') {
        $newDiv = $( "<div id='shapeProperties'><div class='form-group'><label for='widthTextField'>Width, decimal number</label><input type='text' id='widthTextField' name='widthTextField' maxlength='2' class='form-control' placeholder='100'/><br/></div></div>" );
    }
    else if(selectedValue == 'Circle') {
        $newDiv = $( "<div id='shapeProperties'><div class='form-group'><label for='radiusTextField'>Radius, decimal number</label><input type='text' id='radiusTextField' name='radiusTextField' maxlength='2' class='form-control' placeholder='50'/><br/></div></div>" );
    }
    else {
        $newDiv = $( "<div id='shapeProperties'><div class='form-group'><label for='side1TextField'>First side length, decimal number</label><input type='text' id='side1TextField' name='side1TextField' maxlength='2' class='form-control' placeholder='50'/><br/></div>"+
        "<div class='form-group'><label for='side2TextField'>Second side length, decimal number</label><input type='text' id='side2TextField' name='side2TextField' maxlength='2' class='form-control' placeholder='50'/><br/></div></div>" );
    }
    $('#editArea').append($newDiv);
}

function drawShape() {
    var shapeName = $('#formSelect').val();
    //alert('draw');

    var $canvas;
    if(shapeName == 'Quadrat') {
        var $quadrat = {
            id:$shapeIndex,
            color:getColor($('#colorSelector').val()),
            width:parseInt($('#widthTextField').val()),
            type: 'Quadrat'
        };
        $shapesLibrary.push($quadrat);
        $shapeIndex++;
        $canvas = document.createElement('canvas');
        $canvas.id = $quadrat.id;
        $canvas.width = $quadrat.width + 10;
        $canvas.height = $canvas.width;
        var ctx = $canvas.getContext("2d");
        ctx.fillStyle = $quadrat.color;
        ctx.fillRect(20,20,$quadrat.width,$quadrat.width);
    }
    else if(shapeName == 'Circle') {
        var $circle = {
            id:$shapeIndex,
            color:getColor($('#colorSelector').val()),
            radius:parseInt($('#radiusTextField').val()),
            type: 'Circle'
        };
        $shapesLibrary.push($circle);
        $shapeIndex++;
        $canvas = document.createElement('canvas');
        $canvas.id = $circle.id;
        $canvas.width = $circle.radius * 2 + 10;
        $canvas.height = $canvas.width;
        var ctx = $canvas.getContext("2d");
        ctx.fillStyle = $circle.color;
        ctx.beginPath();
        ctx.arc($circle.radius+2, $circle.radius+2, $circle.radius, 0, 2 * Math.PI);
        ctx.fill();
    }
    else {
        var $triangle = {
            id:$shapeIndex,
            color:getColor($('#colorSelector').val()),
            side1Length:parseInt($('#side1TextField').val()),
            side2Length:parseInt($('#side2TextField').val()),
            type: 'Triangle'
        };
        $shapesLibrary.push($triangle);
        $shapeIndex++;
        $canvas = document.createElement('canvas');
        $canvas.id = $triangle.id;
        var $maxLength = $triangle.side1Length > $triangle.side2Length ? $triangle.side1Length : $triangle.side2Length;
        $canvas.width = $maxLength;
        $canvas.height = $canvas.width;
        var ctx = $canvas.getContext("2d");
        ctx.fillStyle = $triangle.color;
        ctx.moveTo(0,0);
        ctx.lineTo(0, $triangle.side1Length);
        ctx.lineTo($triangle.side2Length,$triangle.side1Length);
        ctx.lineTo(0,0);
        ctx.closePath();
        ctx.fill();
    }

    $('#shapes').append($canvas);
    $('#'+$canvas.id).click(function(event){
            var $shape = $shapesLibrary[event.target.id];
            var $textMessage;
            if($shape.type == 'Quadrat') {
                $textMessage = "You've selected one Quadrat[width: "+$shape.width+", x: "+event.pageX+", y: "+event.pageY+"]";
            }
            else if($shape.type == 'Circle') {
                $textMessage = "You've selected one Circle[radius: "+$shape.radius+", x: "+event.pageX+", y: "+event.pageY+"]";
            }
            else {
                $textMessage = "You've selected one Triangle[First side length: "+$shape.side1Length+", second side length: "+$shape.side2Length+", x: "+event.pageX+", y: "+event.pageY+"]";
            }
            alert($textMessage);
    });

    function getColor(value) {
        var $isPresent = value.indexOf('#') === 0;
        if($isPresent) return value;
        else return '#'+value;
    }
}